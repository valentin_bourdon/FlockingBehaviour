# FlockingBehaviour

Implementation of flocking behavior algorithm using ECS and Jobs system from Unity.

[![Watch the video](FlockingBehaviour_poster.png)](FlockingBehaviour.mp4)

## Benchmark

| Nb of boids | Mono | ECS + Jobs |
| ------ | ------ | ------ |
| 100 | ```~59 FPS``` | ```~115 FPS``` |
| 300 | ```~12 FPS``` | ```~115 FPS``` |
| 3000 | ```NaN``` | ```~70 FPS``` |

## License

All the changes made are released under the MIT License, see [LICENSE](./LICENSE).

