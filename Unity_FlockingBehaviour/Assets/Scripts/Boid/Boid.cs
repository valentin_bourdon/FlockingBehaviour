using UnityEngine;
using System.Linq;

public class Boid : MonoBehaviour
{
    [SerializeField]
    private BoidSO _boidSO;
    public BoidSO BoidSO { get => _boidSO; set => _boidSO = value; }

    private Vector3 _direction;
    private Transform _transform;

    private void Awake()
    {
        _transform = transform;
    }
    private void Update()
    {
        Alignment();
        Cohesion();
        Separation();
        AvoidCollision();

        _direction = _direction.normalized * _boidSO.Speed;
        _transform.position += _direction * Time.deltaTime;
        _transform.rotation = Quaternion.LookRotation(_direction, Vector3.up);
    }

    /// <summary>
    /// Rule states that an individual should go towards the mass center of its neighbors.
    /// </summary>
    private void Cohesion()
    {
        Vector3 centroidPosition = Vector3.zero;
        int count = 0;

        foreach (GameObject boid in BoidManager.Instance.Boids)
        {
            if (Vector3.Distance(_transform.position, boid.transform.position) < _boidSO.CohesionRadius)
            {
                centroidPosition += boid.transform.position;
                count++;
            }
        }

        if (count > 0)
        {
            centroidPosition = (centroidPosition / count);

            _direction += (centroidPosition - _transform.position) * _boidSO.CohesionWeigh / 1000;
        }
    }

    /// <summary>
    /// Rule states that an individual should avoid its neighbors when they are close.
    /// </summary>
    private void Separation()
    {
        Vector3 avoidPosition = Vector3.zero;
        int count = 0;

        foreach (GameObject boid in BoidManager.Instance.Boids)
        {
            if (Vector3.Distance(_transform.position, boid.transform.position) < _boidSO.SeparationRadius)
            {
                avoidPosition += _transform.position - boid.transform.position;
                count++;
            }
        }

        if(count > 0)
        {
            _direction += (avoidPosition * _boidSO.SeparationWeigh) / 1000;
        }
    }

    /// <summary>
    /// Rule states that an individual should point to the average direction of its neighbors.
    /// </summary>
    private void Alignment()
    {
        Vector3 alignPosition = Vector3.zero;
        int count = 0;

        foreach (GameObject boid in BoidManager.Instance.Boids)
        {
            if (Vector3.Distance(_transform.position, boid.transform.position) < _boidSO.CohesionRadius)
            {
                alignPosition += boid.transform.forward;
                count++;
            }
        }

        if (count > 0)
        {
            alignPosition = alignPosition / count;
            _direction += (alignPosition * _boidSO.AlignmentWeigh) / 1000;
        }
    }

    /// <summary>
    /// Rule states that an individual should avoid any obstacles in front of him.
    /// </summary>
    private void AvoidCollision()
    {
        _direction += FindUnobstructedDirection() * _boidSO.AvoidCollisionWeigh / 1000;
#if UNITY_EDITOR
        Debug.DrawRay(_transform.position, _direction * _boidSO.CollisionAvoidDistance *1.5f, Color.cyan);
#endif
    }

    /// <summary>
    /// Find any obstacles in front of and choose the best direction.
    /// </summary>
    /// <returns></returns>
    private Vector3 FindUnobstructedDirection()
    {
        Vector3 bestDir = transform.forward;
        float furthestUnobstructedDst = 0;
        RaycastHit hit;

        for (int i = 0; i < BoidManager.Instance.RaysDirection.Count; i++)
        {
            Vector3 dir = _transform.TransformDirection(BoidManager.Instance.RaysDirection[i]);

            if (Physics.SphereCast(_transform.position, _boidSO.BoundRadius, dir, out hit, _boidSO.CollisionAvoidDistance, _boidSO.ObstacleMask))
            {
                if (hit.distance > furthestUnobstructedDst)
                {
                    bestDir = dir;
                    furthestUnobstructedDst = hit.distance;
#if UNITY_EDITOR
                    Debug.DrawRay(_transform.position, bestDir * _boidSO.CollisionAvoidDistance, Color.red);
#endif
                }
            }
            else
            {
#if UNITY_EDITOR
                Debug.DrawRay(_transform.position, dir * _boidSO.CollisionAvoidDistance, Color.green);
#endif
                return dir;
            }
        }
#if UNITY_EDITOR
        Debug.DrawRay(_transform.position, bestDir * _boidSO.CollisionAvoidDistance, Color.green);
#endif

        return bestDir;
    }

    void OnDrawGizmos()
    {
#if UNITY_EDITOR
        DrawCohesionCircle();
        DrawSeparationCircle();
#endif
    }

    void DrawCohesionCircle()
    {
        //Set matrix
        Matrix4x4 defaultMatrix = Gizmos.matrix;
        Gizmos.matrix = transform.localToWorldMatrix;
        //Set color
        Color defaultColor = Gizmos.color;
        Gizmos.color = Color.green;
        //Draw a ring
        Vector3 beginPoint = Vector3.zero;
        Vector3 firstPoint = Vector3.zero;
        for (float theta = 0; theta < 2 * Mathf.PI; theta += 0.1f)
        {
            float x = _boidSO.CohesionRadius * Mathf.Cos(theta);
            float z = _boidSO.CohesionRadius * Mathf.Sin(theta);
            Vector3 endPoint = new Vector3(x, 0, z);
            if (theta == 0)
            {
                firstPoint = endPoint;
            }
            else
            {
                Gizmos.DrawLine(beginPoint, endPoint);
            }
            beginPoint = endPoint;
        }
        //Draw the last segment
        Gizmos.DrawLine(firstPoint, beginPoint);
        //Restore default colors
        Gizmos.color = defaultColor;
        //Restore default matrix
        Gizmos.matrix = defaultMatrix;
    }

    void DrawSeparationCircle()
    {
        //Set matrix
        Matrix4x4 defaultMatrix = Gizmos.matrix;
        Gizmos.matrix = transform.localToWorldMatrix;
        //Set color
        Color defaultColor = Gizmos.color;
        Gizmos.color = Color.red;
        //Draw a ring
        Vector3 beginPoint = Vector3.zero;
        Vector3 firstPoint = Vector3.zero;
        for (float theta = 0; theta < 2 * Mathf.PI; theta += 0.1f)
        {
            float x = _boidSO.SeparationRadius * Mathf.Cos(theta);
            float z = _boidSO.SeparationRadius * Mathf.Sin(theta);
            Vector3 endPoint = new Vector3(x, 0, z);
            if (theta == 0)
            {
                firstPoint = endPoint;
            }
            else
            {
                Gizmos.DrawLine(beginPoint, endPoint);
            }
            beginPoint = endPoint;
        }
        //Draw the last segment
        Gizmos.DrawLine(firstPoint, beginPoint);
        //Restore default colors
        Gizmos.color = defaultColor;
        //Restore default matrix
        Gizmos.matrix = defaultMatrix;
    }
}
