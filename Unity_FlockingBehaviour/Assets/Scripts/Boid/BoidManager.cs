using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BoidManager : MonoBehaviour
{
    public static BoidManager Instance;

    [Serializable]
    struct BoidsType
    {
        public BoidSO boidSO;
        public int spawnCount;
        public float spawnRadius;
        public GameObject spawnTarget;
    }

    [SerializeField]
    private List<BoidsType> _boidsType;

    private List<Vector3> _raysDirection;
    public List<Vector3> RaysDirection { get => _raysDirection; set => _raysDirection = value; }

    private List<GameObject> _boids;
    public List<GameObject> Boids { get => _boids; }

    [SerializeField] int _numberOfRays = 5;
    public int NumberOfRays { get => _numberOfRays; }

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

   
    void Start()
    {
        _boids = new List<GameObject>();
        Spawner();
    }

    private void Spawner()
    {
        for (int i = 0; i < _boidsType.Count; i++)
        {
            RayDirection(_boidsType[i].boidSO);

            for (int j = 0; j < _boidsType[i].spawnCount; j++)
            {
                GameObject boid = Instantiate(_boidsType[i].boidSO.Prefab, RandomPointOnCircle(_boidsType[i].spawnTarget.transform.position, _boidsType[i].spawnRadius), Quaternion.identity);
                boid.GetComponent<Boid>().BoidSO = _boidsType[i].boidSO;

                _boids.Add(boid);
            }
        }
    }


    private Vector3 RandomPointOnCircle(Vector3 startPos, float radius)
    {
        Vector2 randomPoint = UnityEngine.Random.insideUnitCircle.normalized * radius;

        // return random point on circle, centered around the player position
        return new Vector3(randomPoint.x, 0.5f, randomPoint.y) + startPos;
    }

    /// <summary>
    /// Create rays arround a point
    /// </summary>
    /// <param name="boidSO"></param>
    private void RayDirection(BoidSO boidSO)
    {
        _raysDirection = new List<Vector3>();

        float goldenRatio = (1 + Mathf.Sqrt(5)) / 2;
        float angleIncrement = Mathf.PI * 2 * goldenRatio;

        for (int i = 0; i < _numberOfRays; i++)
        {
            float t = (float)i / _numberOfRays;
            float inclination = Mathf.Acos(1 - 2 * t);
            float azimuth = angleIncrement * i;

            float x = Mathf.Sin(inclination) * Mathf.Cos(azimuth);
            float y = Mathf.Sin(inclination) * Mathf.Sin(azimuth);
            float z = Mathf.Cos(inclination);
            _raysDirection.Add(new Vector3(x, y, z));
        }
    }
}
