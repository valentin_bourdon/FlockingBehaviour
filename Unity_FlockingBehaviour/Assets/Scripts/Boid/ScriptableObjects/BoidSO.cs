using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BoidSO", menuName = "BoidSystem/Boid", order = 1)]
public class BoidSO : ScriptableObject
{
    [Header("Prefab")]
    [SerializeField]
    private GameObject _boidPrefab;
    public GameObject Prefab { get => _boidPrefab; }

    [Header("Speed")]
    [SerializeField] float _speed = 4f;
    public float Speed { get => _speed; }

    [Header("Cohesion")]

    [SerializeField] float _cohesionRadius = 5f;
    public float CohesionRadius { get => _cohesionRadius; }

    [SerializeField] float _cohesionWeigh = 5f;
    public float CohesionWeigh { get => _cohesionWeigh; }

    [Header("Separation")]

    [SerializeField] float _separationRadius = 1.5f;
    public float SeparationRadius { get => _separationRadius; }

    [SerializeField] float _separationWeigh = 5f;
    public float SeparationWeigh { get => _separationWeigh; }

    [Header("Alignment")]

    [SerializeField] float _alignmentWeigh = 5f;
    public float AlignmentWeigh { get => _alignmentWeigh; }

    [Header("Collision")]

    [SerializeField] float _boundRadius = 5f;
    public float BoundRadius { get => _boundRadius; }

    [SerializeField] float _collisionAvoidDist = 5f;
    public float CollisionAvoidDistance { get => _collisionAvoidDist; }

    [SerializeField] float _avoidCollisionWeigh = 5f;
    public float AvoidCollisionWeigh { get => _avoidCollisionWeigh; }

    [SerializeField] private LayerMask _obstacleMask;
    public LayerMask ObstacleMask { get => _obstacleMask; }
}
