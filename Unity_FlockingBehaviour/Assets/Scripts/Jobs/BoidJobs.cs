using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

public class BoidJobs : MonoBehaviour
{
    [Header("Mesh")]
    [SerializeField] private Mesh sharedMesh;
    [SerializeField] private List<Material> sharedMaterial;

    [Header("Speed")]
    [SerializeField]
    private float _speed;
    public float Speed => _speed;

    [Header("Collision")]
    [SerializeField]
    private float _collisionAvoidDist;
    public float CollisionAvoidDist => _collisionAvoidDist;
    [SerializeField]
    private float _avoidCollisionWeigh;
    public float AvoidCollisionWeigh => _avoidCollisionWeigh;

    [Header("Boids")]
    private NativeArray<float3> _direction;
    public NativeArray<float3> Direction => _direction;

    private NativeArray<float3> _collisionDir;
    public NativeArray<float3> CollisionDir => _collisionDir;

    private NativeArray<Entity> _entities;
    public NativeArray<Entity> Entities => _entities;

    private NativeArray<LocalToWorld> _localToWorlds;
    public NativeArray<LocalToWorld> LocalToWorlds => _localToWorlds;


    [Serializable]
    struct BoidsType
    {
        public int spawnCount;
        public float spawnRadius;
        public GameObject spawnTarget;
    }

    [SerializeField]
    private List<BoidsType> _boidsType;

    [Header("Rays Parameters")]
    [SerializeField]
    private int _numberOfRays;
    [ReadOnly]
    public NativeArray<float3> _raysDirection;
    public NativeArray<float3> Rays { get => _raysDirection; }

    public EntityManager entityManager; //An EntityManager is a class to process Entities and their data.
    private EntityArchetype entityArchetype;

    public virtual void Start()
    {
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        _direction = new NativeArray<float3>(NumberOfBoids(), Allocator.Persistent);
        _collisionDir = new NativeArray<float3>(NumberOfBoids(), Allocator.Persistent);
        _entities = new NativeArray<Entity>(NumberOfBoids(), Allocator.Persistent);
        _localToWorlds = new NativeArray<LocalToWorld>(NumberOfBoids(), Allocator.Persistent);

        RayDirection();

        entityArchetype = entityManager.CreateArchetype(
            typeof(RenderMesh),
            typeof(RenderBounds),
            typeof(LocalToWorld)
        );

        Spawner();
    }

    /// <summary>
    /// Spawn entities arround a circle
    /// Add component data
    /// </summary>
    private void Spawner()
    {
        int index = 0;

        for (int i = 0; i < _boidsType.Count; i++)
        {
            for (int j = 0; j < _boidsType[i].spawnCount; j++)
            {
                Entity e = entityManager.CreateEntity(entityArchetype);

                entityManager.AddComponentData(e, new LocalToWorld
                {
                    Value = float4x4.TRS(
                    RandomPointOnCircle(_boidsType[i].spawnTarget.transform.position, _boidsType[i].spawnRadius),
                    Quaternion.identity,
                    new float3(1f))
                });

                entityManager.SetComponentData(e, new RenderBounds
                {
                    Value = sharedMesh.bounds.ToAABB()
                });

                entityManager.AddSharedComponentData(e, new RenderMesh
                {
                    mesh = sharedMesh,
                    material = sharedMaterial[UnityEngine.Random.Range(0,sharedMaterial.Count)],
                    castShadows = UnityEngine.Rendering.ShadowCastingMode.On,
                    layerMask = 1,
                    layer = 5
                });

                _entities[index] = e;
                _localToWorlds[index] = entityManager.GetComponentData<LocalToWorld>(e);
                _direction[index] = new float3(0, 0, 0);
                index++;
            }
        }
    }

    /// <summary>
    /// Find a point arround a circle
    /// </summary>
    /// <param name="startPos"></param>
    /// <param name="radius"></param>
    /// <returns></returns>
    private Vector3 RandomPointOnCircle(Vector3 startPos, float radius)
    {
        Vector3 randomPoint = UnityEngine.Random.insideUnitCircle.normalized * radius;

        // return random point on circle, centered around the player position
        return new Vector3(randomPoint.x, randomPoint.y, randomPoint.y) + startPos;
    }

    /// <summary>
    /// Create rays arround a point
    /// </summary>
    private void RayDirection()
    {
        _raysDirection = new NativeArray<float3>(_numberOfRays, Allocator.Persistent);

        float goldenRatio = (1 + math.sqrt(5)) / 2;
        float angleIncrement = math.PI * 2 * goldenRatio;

        for (int i = 0; i < _numberOfRays; i++)
        {
            float t = (float)i / _numberOfRays;
            float inclination = math.acos(1 - 2 * t);
            float azimuth = angleIncrement * i;

            float x = math.sin(inclination) * math.cos(azimuth);
            float y = math.sin(inclination) * math.sin(azimuth);
            float z = math.cos(inclination);
            _raysDirection[i] = new float3(x, y, z);
        }
    }

    public int NumberOfBoids()
    {
        int count = 0;
        for (int i = 0; i < _boidsType.Count; i++)
        {
            for (int j = 0; j < _boidsType[i].spawnCount; j++)
            {
                count++;
            }
        }

        return count;
    }
    public virtual void OnDestroy()
    {
        _raysDirection.Dispose();
        _entities.Dispose();
        _localToWorlds.Dispose();
        _direction.Dispose();
        _collisionDir.Dispose();
    }
}
