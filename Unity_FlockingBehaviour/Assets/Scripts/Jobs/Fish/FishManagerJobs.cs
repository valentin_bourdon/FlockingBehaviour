using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

public class FishManagerJobs : BoidJobs
{
    public static FishManagerJobs Instance;

    [Header("Alignement")]
    [SerializeField]
    private float _alignmentRadius;
    public float AlignmentRadius => _alignmentRadius;
    [SerializeField]
    private float _alignmentWeigh;
    public float AlignmentWeigh => _alignmentWeigh;

    [Header("Cohesion")]
    [SerializeField]
    private float _cohesionRadius;
    public float CohesionRadius => _cohesionRadius;
    [SerializeField]
    private float _cohesionWeigh;
    public float CohesionWeigh => _cohesionWeigh;

    [Header("Separation")]
    [SerializeField]
    public float _separationRadius;
    public float SeparationRadius => _separationRadius;
    [SerializeField]
    private float _separationWeigh;
    public float SeparationWeigh => _separationWeigh;

    [Header("Escape")]
    [SerializeField]
    private float _escapeWeigh;
    public float EscapeWeigh => _escapeWeigh;
    [SerializeField]
    private float _distToEscape;
    public float DistToEscape => _distToEscape;

    [Header("Boids")]
    private NativeArray<float3> _escapeDir;
    public NativeArray<float3> EscapeDir => _escapeDir;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public override void Start()
    {
        base.Start();

        _escapeDir = new NativeArray<float3>(NumberOfBoids(), Allocator.Persistent);
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
        _escapeDir.Dispose();
    }
}
