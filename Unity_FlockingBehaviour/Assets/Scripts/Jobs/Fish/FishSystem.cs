using Unity.Mathematics;
using Unity.Entities;
using Unity.Transforms;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;
using UnityEngine;
using RaycastHit = Unity.Physics.RaycastHit;

public partial class FishSystem : SystemBase
{
    //Flocking Job
    private JobHandle _flockingJobHandle;
    private UpdateFlockingJob _flockingJob;

    //Raycast Job
    private JobHandle _raycastJobHandle;
    private RaycastJob _raycastJob;

    //Raycast Job
    private JobHandle _escapeJobHandle;
    private EscapeJob _escapeJob;

    private static BuildPhysicsWorld _physicsWorldSystem;
    private static CollisionWorld _collisionWorld;

    private CollisionFilter _collisionFilter;

    protected override void OnCreate()
    {
        _physicsWorldSystem = World.GetOrCreateSystem<BuildPhysicsWorld>();
        _collisionWorld = _physicsWorldSystem.PhysicsWorld.CollisionWorld;

        _collisionFilter = new CollisionFilter()
        {
            BelongsTo = ~0u,
            CollidesWith = ~0u, // all 1s, so all layers, collide with everything
            GroupIndex = 0
        };
    }

    protected override void OnUpdate()
    {
        if (FishManagerJobs.Instance == null || FishManagerJobs.Instance.Entities.Length == 0)
            return;

        _collisionWorld = _physicsWorldSystem.PhysicsWorld.CollisionWorld;
        World.GetOrCreateSystem<StepPhysicsWorld>().FinalSimulationJobHandle.Complete();

        _escapeJob = new EscapeJob()
        {
            skarksPosition = SharkManagerJobs.Instance.LocalToWorlds,
            fishPosition = FishManagerJobs.Instance.LocalToWorlds,
            escapeDir = FishManagerJobs.Instance.EscapeDir,
            distToEscape = FishManagerJobs.Instance.DistToEscape
        };

        _escapeJobHandle = _escapeJob.Schedule(FishManagerJobs.Instance.Entities.Length, 1);
        _escapeJobHandle.Complete();

        _raycastJob = new RaycastJob
        {
            collisionAvoidDist = FishManagerJobs.Instance.CollisionAvoidDist,
            bestDir = FishManagerJobs.Instance.CollisionDir,
            world = _collisionWorld,
            localToWorld = FishManagerJobs.Instance.LocalToWorlds,
            raysDirection = FishManagerJobs.Instance.Rays,
            collisonFilter = _collisionFilter
        };
        _raycastJobHandle = _raycastJob.Schedule(FishManagerJobs.Instance.Entities.Length, 1, _escapeJobHandle);
        _raycastJobHandle.Complete();

        _flockingJob = new UpdateFlockingJob()
        {
            world = _collisionWorld,
            direction = FishManagerJobs.Instance.Direction,
            localToWorlds = FishManagerJobs.Instance.LocalToWorlds,
            entities = FishManagerJobs.Instance.Entities,
            speed = FishManagerJobs.Instance.Speed,
            alignmentWeigh = FishManagerJobs.Instance.AlignmentWeigh,
            alignmentRadius = FishManagerJobs.Instance.AlignmentRadius,
            collisionAvoidDist = FishManagerJobs.Instance.CollisionAvoidDist,
            avoidCollisionWeigh = FishManagerJobs.Instance.AvoidCollisionWeigh,
            cohesionRadius = FishManagerJobs.Instance.CohesionRadius,
            cohesionWeigh = FishManagerJobs.Instance.CohesionWeigh,
            separationRadius = FishManagerJobs.Instance.SeparationRadius,
            separationWeigh = FishManagerJobs.Instance.SeparationWeigh,
            escapeWeigh = FishManagerJobs.Instance.EscapeWeigh,
            avoidCollisionDir = _raycastJob.bestDir,
            time = Time.DeltaTime,
            escapeDir = _escapeJob.escapeDir
        };

        _flockingJobHandle = _flockingJob.Schedule(FishManagerJobs.Instance.Entities.Length, 1, _escapeJobHandle);
        _flockingJobHandle.Complete();

        for (int i = 0; i < FishManagerJobs.Instance.Entities.Length; i++)
        {
            FishManagerJobs.Instance.entityManager.SetComponentData(FishManagerJobs.Instance.Entities[i], new LocalToWorld
            {
                Value = _flockingJob.localToWorlds[i].Value
            });
        }
    }

    [BurstCompile]
    private struct UpdateFlockingJob : IJobParallelFor
    {
        [ReadOnly] public CollisionWorld world;

        public NativeArray<float3> direction;

        [ReadOnly]
        public NativeArray<Entity> entities;

        [NativeDisableParallelForRestriction]
        public NativeArray<LocalToWorld> localToWorlds;

        [ReadOnly]
        public NativeArray<float3> avoidCollisionDir;

        [ReadOnly]
        public NativeArray<float3> escapeDir;

        [ReadOnly]
        public float speed;
        [ReadOnly]
        public float alignmentWeigh;
        [ReadOnly]
        public float alignmentRadius;
        [ReadOnly]
        public float collisionAvoidDist;
        [ReadOnly]
        public float avoidCollisionWeigh;
        [ReadOnly]
        public float cohesionRadius;
        [ReadOnly]
        public float cohesionWeigh;
        [ReadOnly]
        public float separationRadius;
        [ReadOnly]
        public float separationWeigh;
        [ReadOnly]
        public float escapeWeigh;
        [ReadOnly]
        public float time;

        float3 cohesionSum;
        float3 alignmentSum;
        float3 separationSum;

        int cohesionCount;
        int alignmentCount;
        int separationCount;

        public void Execute(int i)
        {
            cohesionCount = 0;
            alignmentCount = 0;
            separationCount = 0;
            cohesionSum = float3.zero;
            alignmentSum = float3.zero;
            separationSum = float3.zero;
           
            if(math.all(escapeDir[i] != float3.zero))
            {
                //Escape
                direction[i] += escapeDir[i] * escapeWeigh / 1000;
            }
            else
            {
                for (int j = 0; j < entities.Length; j++)
                {
                    if (entities[i] != entities[j])
                    {
                        //Cohesion
                        if (math.distance(localToWorlds[i].Position, localToWorlds[j].Position) < cohesionRadius)
                        {
                            cohesionSum += localToWorlds[j].Position;
                            cohesionCount++;
                        }

                        //Alignment
                        if (math.distance(localToWorlds[i].Position, localToWorlds[j].Position) < alignmentRadius)
                        {
                            alignmentSum += localToWorlds[j].Forward;
                            alignmentCount++;
                        }

                        //Separation
                        if (math.distance(localToWorlds[i].Position, localToWorlds[j].Position) < separationRadius)
                        {
                            separationSum += localToWorlds[i].Position - localToWorlds[j].Position;
                            separationCount++;
                        }
                    }
                }

                if (cohesionCount > 0)
                {
                    //Cohesion
                    cohesionSum = (cohesionSum / cohesionCount);
                    direction[i] += (cohesionSum - localToWorlds[i].Position) * cohesionWeigh / 1000;
                }

                if (alignmentCount > 0)
                {
                    //Alignment
                    alignmentSum = (alignmentSum / alignmentCount);
                    direction[i] += (alignmentSum * alignmentWeigh) / 1000;
                }

                if (separationCount > 0)
                {
                    //Separation
                    direction[i] += (separationSum * separationWeigh) / 1000;
                }
            }
           
            //Obtacles
            direction[i] += avoidCollisionDir[i] * avoidCollisionWeigh / 1000;

            direction[i] = math.normalize(direction[i]) * speed;

            localToWorlds[i] = new LocalToWorld
            {
                Value = float4x4.TRS(
                    localToWorlds[i].Position + (direction[i] * time),
                    quaternion.LookRotationSafe(direction[i], math.up()),
                    new float3(1f))

            };
        }
    }

    [BurstCompile]
    public struct RaycastJob : IJobParallelFor
    {
        [ReadOnly] public CollisionWorld world;

        [ReadOnly]
        public NativeArray<LocalToWorld> localToWorld;

        [ReadOnly]
        public NativeArray<float3> raysDirection;

        [ReadOnly]
        public CollisionFilter collisonFilter;

        [ReadOnly]
        public float collisionAvoidDist;

        public NativeArray<float3> bestDir;

        public void Execute(int index)
        {
            bestDir[index] = localToWorld[index].Forward;
            float furthestUnobstructedDst = 0;
            
            for (int i = 0; i < raysDirection.Length; i++)
            {
                Matrix4x4 matrixTrans = Matrix4x4.identity;
                matrixTrans.SetTRS(localToWorld[index].Position, localToWorld[index].Rotation, new float3(1.0f, 1.0f, 1.0f));
                float3 dir = matrixTrans.MultiplyVector(raysDirection[i]);

                RaycastInput input = new RaycastInput()
                {
                    Start = localToWorld[index].Position,
                    End = localToWorld[index].Position + (dir * collisionAvoidDist),
                    Filter = collisonFilter
                };

                RaycastHit hit = new RaycastHit();
                bool haveHit = world.CastRay(input, out hit);

                if (haveHit)
                {
                    float dist = math.distance(localToWorld[index].Position, hit.Position);
#if UNITY_EDITOR
                    Debug.DrawRay(localToWorld[index].Position, (localToWorld[index].Position - hit.Position) * collisionAvoidDist, Color.red);
#endif
                    if (dist > furthestUnobstructedDst)
                    {
                        bestDir[index] = dir;
                        furthestUnobstructedDst = dist;
#if UNITY_EDITOR     
                        Debug.DrawRay(localToWorld[index].Position, bestDir[index] * collisionAvoidDist, Color.blue);
#endif
                    }
                }
                else
                {
#if UNITY_EDITOR                    
                    Debug.DrawRay(localToWorld[index].Position, dir * collisionAvoidDist, Color.green);
#endif
                    bestDir[index] = dir;
                    break;
                }
            }
        }
    }

    [BurstCompile]
    public struct EscapeJob : IJobParallelFor
    {
        [NativeDisableParallelForRestriction]
        public NativeArray<LocalToWorld> skarksPosition;

        [NativeDisableParallelForRestriction]
        public NativeArray<LocalToWorld> fishPosition;

        public NativeArray<float3> escapeDir;

        [ReadOnly]
        public float distToEscape;

        public void Execute(int index)
        {
            for (int i = 0; i < skarksPosition.Length; i++)
            {
                float dist = math.distance(skarksPosition[i].Position, fishPosition[index].Position);
                if (dist < distToEscape)
                {
                    escapeDir[index] = fishPosition[index].Position - skarksPosition[i].Position;
#if UNITY_EDITOR
                    Debug.DrawRay(fishPosition[index].Position, escapeDir[index] * dist, Color.black);
#endif
                    break;
                }
                else
                {
                    escapeDir[index] = float3.zero;
                    break;
                }
            }
        }
    }
}
