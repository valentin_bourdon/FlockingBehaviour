
using UnityEngine;

public class SharkManagerJobs : BoidJobs
{
    public static SharkManagerJobs Instance;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }
}
