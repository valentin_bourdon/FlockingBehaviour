using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using RaycastHit = Unity.Physics.RaycastHit;

public partial class SharkSystem : SystemBase
{
    //Flocking Job
    private JobHandle _flockingJobHandle;
    private UpdateFlockingJob _flockingJob;

    //Raycast Job
    private JobHandle _raycastJobHandle;
    private RaycastJob _raycastJob;

    private static BuildPhysicsWorld _physicsWorldSystem;
    private static CollisionWorld _collisionWorld;

    private CollisionFilter _collisionFilter;

    protected override void OnCreate()
    {
        _physicsWorldSystem = World.GetOrCreateSystem<BuildPhysicsWorld>();
        _collisionWorld = _physicsWorldSystem.PhysicsWorld.CollisionWorld;

        _collisionFilter = new CollisionFilter()
        {
            BelongsTo = ~0u,
            CollidesWith = ~0u, // all 1s, so all layers, collide with everything
            GroupIndex = 0
        };
    }

    protected override void OnUpdate()
    {
        if (SharkManagerJobs.Instance == null || SharkManagerJobs.Instance.Entities.Length == 0)
            return;

        _collisionWorld = _physicsWorldSystem.PhysicsWorld.CollisionWorld;
        World.GetOrCreateSystem<StepPhysicsWorld>().FinalSimulationJobHandle.Complete();

        _raycastJob = new RaycastJob
        {
            collisionAvoidDist = SharkManagerJobs.Instance.CollisionAvoidDist,
            bestDir = SharkManagerJobs.Instance.CollisionDir,
            world = _collisionWorld,
            localToWorld = SharkManagerJobs.Instance.LocalToWorlds,
            raysDirection = SharkManagerJobs.Instance.Rays,
            collisonFilter = _collisionFilter
        };
        _raycastJobHandle = _raycastJob.Schedule(SharkManagerJobs.Instance.Entities.Length, 1);
        _raycastJobHandle.Complete();

        _flockingJob = new UpdateFlockingJob()
        {
            direction = SharkManagerJobs.Instance.Direction,
            localToWorlds = SharkManagerJobs.Instance.LocalToWorlds,
            speed = SharkManagerJobs.Instance.Speed,
            avoidCollisionWeigh = SharkManagerJobs.Instance.AvoidCollisionWeigh,
            avoidCollisionDir = _raycastJob.bestDir,
            time = Time.DeltaTime
        };

        _flockingJobHandle = _flockingJob.Schedule(SharkManagerJobs.Instance.Entities.Length, 1, _raycastJobHandle);
        _flockingJobHandle.Complete();

        for (int i = 0; i < SharkManagerJobs.Instance.Entities.Length; i++)
        {
            SharkManagerJobs.Instance.entityManager.SetComponentData(SharkManagerJobs.Instance.Entities[i], new LocalToWorld
            {
                Value = _flockingJob.localToWorlds[i].Value
            });
        }
    }

    [BurstCompile]
    private struct UpdateFlockingJob : IJobParallelFor
    {
        public NativeArray<float3> direction;

        [NativeDisableParallelForRestriction]
        public NativeArray<LocalToWorld> localToWorlds;

        [ReadOnly]
        public NativeArray<float3> avoidCollisionDir;

        [ReadOnly]
        public float speed;
        [ReadOnly]
        public float avoidCollisionWeigh;
        [ReadOnly]
        public float time;


        public void Execute(int i)
        {
            //Obtacles
            direction[i] += avoidCollisionDir[i] * avoidCollisionWeigh / 1000;

            direction[i] = math.normalize(direction[i]) * speed;

            localToWorlds[i] = new LocalToWorld
            {
                Value = float4x4.TRS(
                    localToWorlds[i].Position + (direction[i] * time),
                    quaternion.LookRotationSafe(direction[i], math.up()),
                    new float3(1f))

            };
        }
    }

    [BurstCompile]
    public struct RaycastJob : IJobParallelFor
    {
        [ReadOnly] public CollisionWorld world;

        [ReadOnly]
        public NativeArray<LocalToWorld> localToWorld;

        [ReadOnly]
        public NativeArray<float3> raysDirection;

        [ReadOnly]
        public CollisionFilter collisonFilter;

        [ReadOnly]
        public float collisionAvoidDist;

        public NativeArray<float3> bestDir;

        public void Execute(int index)
        {
            bestDir[index] = localToWorld[index].Forward;
            float furthestUnobstructedDst = 0;

            for (int i = 0; i < raysDirection.Length; i++)
            {
                Matrix4x4 matrixTrans = Matrix4x4.identity;
                matrixTrans.SetTRS(localToWorld[index].Position, localToWorld[index].Rotation, new float3(1.0f, 1.0f, 1.0f));
                float3 dir = matrixTrans.MultiplyVector(raysDirection[i]);

                RaycastInput input = new RaycastInput()
                {
                    Start = localToWorld[index].Position,
                    End = localToWorld[index].Position + (dir * collisionAvoidDist),
                    Filter = collisonFilter
                };

                RaycastHit hit = new RaycastHit();
                bool haveHit = world.CastRay(input, out hit);
                
                if (haveHit)
                {
                    float dist = math.distance(localToWorld[index].Position, hit.Position);
#if UNITY_EDITOR
                    Debug.DrawRay(localToWorld[index].Position, (localToWorld[index].Position - hit.Position) * collisionAvoidDist, Color.red);
#endif
                    if (dist > furthestUnobstructedDst)
                    {
                        bestDir[index] = dir;
                        furthestUnobstructedDst = dist;
#if UNITY_EDITOR     
                        Debug.DrawRay(localToWorld[index].Position, bestDir[index] * collisionAvoidDist, Color.red);
#endif
                    }
                }
                else
                {
#if UNITY_EDITOR                    
                    Debug.DrawRay(localToWorld[index].Position, dir * collisionAvoidDist, Color.blue);
#endif
                    bestDir[index] = dir;
                    break;
                }
            }
        }
    }
}
